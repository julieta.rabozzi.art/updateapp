import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:update_app/main.dart';
import 'package:update_app/service/firebase_remote_config_service.dart';

class MockRemoteConfigService extends Mock implements FirebaseRemoteConfigService {}

void main() {
  setUpAll(() {
    //para trabajar con Navigator 
    registerFallbackValue(FakeRoute());
  });

  testWidgets('Se muestra modal y boton', (WidgetTester tester) async {
    final mockRemoteConfigService = MockRemoteConfigService();
    
    when(() => mockRemoteConfigService.initialize()).thenAnswer((_) async => {});
    when(() => mockRemoteConfigService.enabled).thenReturn(true);
    when(() => mockRemoteConfigService.isOptional).thenReturn(true);
    when(() => mockRemoteConfigService.getMensaje()).thenAnswer((_) async => 'Actualizacion Disponible');

    await tester.pumpWidget(MyApp(remoteConfigService: mockRemoteConfigService));
    await tester.pumpAndSettle();

    // Verifica si el diálogo aparece cuando enabled es true
    expect(find.byType(AlertDialog), findsOneWidget);
    // Verifica si el botón 'Cerrar' aparece cuando isOptional es true
    expect(find.text('Cerrar'), findsOneWidget);
  });

  testWidgets('No se muestra modal', (WidgetTester tester) async {
    final mockRemoteConfigService = MockRemoteConfigService();
    
    when(() => mockRemoteConfigService.initialize()).thenAnswer((_) async => {});
    when(() => mockRemoteConfigService.enabled).thenReturn(false);
    when(() => mockRemoteConfigService.isOptional).thenReturn(true);
    when(() => mockRemoteConfigService.getMensaje()).thenAnswer((_) async => 'Actualizacion Disponible');

    await tester.pumpWidget(MyApp(remoteConfigService: mockRemoteConfigService));
    await tester.pumpAndSettle();

    // Verifica si el diálogo no aparece cuando enabled es false
    expect(find.byType(AlertDialog), findsNothing);
  });

  testWidgets('no se muestra boton Cerrar', (WidgetTester tester) async {
    final mockRemoteConfigService = MockRemoteConfigService();
    
    when(() => mockRemoteConfigService.initialize()).thenAnswer((_) async => {});
    when(() => mockRemoteConfigService.enabled).thenReturn(true);
    when(() => mockRemoteConfigService.isOptional).thenReturn(false);
    when(() => mockRemoteConfigService.getMensaje()).thenAnswer((_) async => 'Actualizacion Disponible');

    await tester.pumpWidget(MyApp(remoteConfigService: mockRemoteConfigService));
    await tester.pumpAndSettle();

    // Verifica si el botón 'Cerrar' no aparece cuando isOptional es false
    expect(find.text('Cerrar'), findsNothing);
  });
}

class FakeRoute extends Fake implements Route<void> {}
