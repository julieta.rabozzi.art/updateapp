import 'package:flutter/material.dart';
import 'package:update_app/service/firebase_remote_config_service.dart';
import 'package:url_launcher/url_launcher.dart';
//manejar la logica de la actualizacion
class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.remoteConfigService}) : super(key: key);
  final FirebaseRemoteConfigService remoteConfigService;

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String>(
      future: widget.remoteConfigService.getMensaje(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        }

        String mensaje = snapshot.data ?? 'No se muestra mensaje';

        if (widget.remoteConfigService.enabled) {
          WidgetsBinding.instance.addPostFrameCallback((_){
            showDialog(
              context: context, 
              barrierDismissible: widget.remoteConfigService.isOptional,
              builder: (context) {
                return AlertDialog(
                  title: const Text('Actualizacion Disponible'),
                  content: Text(mensaje),
                  actions: <Widget>[
                    if (widget.remoteConfigService.isOptional)
                      TextButton(
                        child: const Text('Cerrar'),
                        onPressed: () => Navigator.of(context).pop(),
                      ),
                    TextButton(
                      child: const Text('Actualizar'),
                      onPressed: () async {
                        String url = widget.remoteConfigService.appUrl;
                        if (await canLaunch(url)) {
                          await launch(url, forceSafariVC: false);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                    ),
                  ],
                );
              },
            );
          });
        }

        return Scaffold(
          appBar: AppBar(
            title: const Text('Home Page'),
          ),
          body: const Center(
            child: Text('Hello World!'),
          ),
        );
      },
    );
  }
}
