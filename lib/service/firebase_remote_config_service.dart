import 'dart:convert';
import 'dart:io';  // para usar Platform
import 'package:firebase_remote_config/firebase_remote_config.dart';

class FirebaseRemoteConfigService {
  FirebaseRemoteConfigService._privateConstructor();
  static final FirebaseRemoteConfigService instance = FirebaseRemoteConfigService._privateConstructor();

  FirebaseRemoteConfig _remoteConfig = FirebaseRemoteConfig.instance;

  set remoteConfig(FirebaseRemoteConfig remoteConfig) {
    _remoteConfig = remoteConfig;
  }

  set enabled(bool value) {
    enabled = value;
  }

    set isOptional(bool value) {
    enabled = value;
  }

  Future<void> initialize() async {
    try {
      await _remoteConfig.setConfigSettings(RemoteConfigSettings(
        fetchTimeout: const Duration(seconds: 10),
        minimumFetchInterval: const Duration(hours: 1),
      ));

      await _remoteConfig.setDefaults(<String, dynamic>{
        'esAndroid': '{"version": "3.10.0", "isOptional": false, "enabled": true, "appUrl": "https://play.google.com/store/apps/details?id=com.roblox.client"}',
        'esIOS': '{"version": "3.10.0", "isOptional": false, "enabled": true, "appUrl": "https://apps.apple.com/us/app/roblox/id431946152"}',
      });

      await checkForUpdates();
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be used');
    }
  }

  Future<void> checkForUpdates() async {
    await _remoteConfig.fetchAndActivate();
  }

  Map<String, dynamic> get config {
    String configStr;
    if (Platform.isAndroid) {
      configStr = _remoteConfig.getString('esAndroid');
    } else if (Platform.isIOS) {
      configStr = _remoteConfig.getString('esIOS');
    } else {
      // En este caso, podrías definir un valor por defecto para otras plataformas
      configStr = '{"version": "3.10.0", "isOptional": false, "enabled": true}';
    }
    return jsonDecode(configStr);
  }

Future<String> getMensaje() async {
    try {
      // Fetch and activate the latest config.
      await _remoteConfig.fetchAndActivate();
      // Get the message.
      String mensaje = _remoteConfig.getString('mensaje');
      return mensaje;
    } catch (e) {
      print('Failed to fetch modal mensaje. ' + e.toString());
      return 'Default modal mensaje';
    }
  }


  String get version => config['version'];
  bool get enabled => config['enabled'];
  bool get isOptional => config['isOptional'];
  String get appUrl => config['appUrl'];
}
