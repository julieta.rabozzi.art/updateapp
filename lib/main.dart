import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:update_app/service/firebase_remote_config_service.dart';

import 'firebase_options.dart';
import 'home_page.dart'; 
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  FirebaseRemoteConfigService remoteConfigService = FirebaseRemoteConfigService.instance;
  await remoteConfigService.initialize();
  
  runApp(MyApp(remoteConfigService: remoteConfigService));  
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key, required this.remoteConfigService}) : super(key: key);
  final FirebaseRemoteConfigService remoteConfigService;
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Upgrader Example',
      home: HomePage(remoteConfigService: remoteConfigService),
    );
  }
}
